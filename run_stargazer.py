import os
import pandas as pd
import subprocess

BIN='/home/nat/Documents/tools/Stargazer_v1.0.8/Stargazer_v1.0.8'
ref_bin='/home/nat/Documents/tools/ref'
gene_list = ['cftr', 'ugt2b17', 'gstt1', 'nat2', 'cyp3a43', 'cyp2j2', 'sult1a1',
'slco2b1', 'ugt2b7', 'nudt15', 'slc15a2', 'cyp2c19', 'cyp2e1', 'cyp3a7','ifnl3',
'ugt1a4', 'cyp2s1', 'g6pd', 'dpyd', 'cyp2b6', 'cyp3a5', 'slco1b1','cacna1s',
'tpmt', 'cyp1a2', 'cyp26a1', 'vkorc1', 'ugt1a1', 'cyp2a13', 'gstm1','cyp2c8',
'slc22a2', 'ryr1', 'cyp2a6', 'cyp2d6', 'ugt2b15', 'tbxas1', 'cyp4b1','cyp1a1',
'cyp19a1', 'cyp1b1', 'cyp2f1', 'por', 'cyp2c9', 'cyp3a4', 'cyp4f2','cyp2r1',
'cyp2w1', 'gstp1', 'slco1b3', 'nat1']
# gene_list_custom = ['por', 'cyp2c9', 'cyp3a4', 'cyp4f2','cyp2r1',
# 'cyp2w1', 'gstp1', 'slco1b3', 'nat1']

def clean(ID):
    os.remove(f"{ID}/{ID}.gdf")
    os.remove(f"{ID}/{ID}.vcf.gz")
    os.remove(f"{ID}/{ID}.vcf.gz.tbi")
    return

def clear(ID,gene):
    os.remove(f"{ID}/{gene}.stargazer-genotype.log")
    shutil.rmtree(f"{ID}/{gene}.stargazer-genotype.project",ignore_errors=True)
    return

def extract_vcf(bam,ID):
    subprocess.run(['gatk','--java-options',"-Xmx4g",'HaplotypeCaller',
    '-R',f'{ref_bin}/hg19/Homo_sapiens_assembly19.fasta','-I', bam,
    '-O',f'{ID}/{ID}.vcf.gz','-L','stargazer_pgx.bed','--verbosity','ERROR'])
    return

def extract_gdf(bam,ID):
    subprocess.run(['gatk','--java-options',"-Xmx4g",'DepthOfCoverage',
    '-R',f'{ref_bin}/hg19/Homo_sapiens_assembly19.fasta','-I', bam,
    '-O',f'{ID}/{ID}.gdf','-L','stargazer_pgx.bed','--verbosity','ERROR',
    '--omit-genes-not-entirely-covered-by-traversal','--omit-locus-table',
    '--omit-interval-statistics','--omit-per-sample-statistics'])
    return

def csv2tsv(file):
    df = pd.read_csv(file)
    df.to_csv(file,sep='\t',index=False)
    return

def run(file):
    ID = os.path.basename(file)[:-4]
    if not os.path.exists(ID):
        os.mkdir(ID)
    extract_vcf(file,ID)
    extract_gdf(file,ID)
    csv2tsv(f'{ID}/{ID}.gdf')
    for gene in gene_list:
        try:
            subprocess.run(['python',f'{BIN}/stargazer.py','genotype',
            '-o',f'{ID}/{gene}','-d','wgs','-t',gene,'--vcf',f'{ID}/{ID}.vcf.gz',
            '-c','vdr','--gdf',f'{ID}/{ID}.gdf'],timeout=150)
        except:
            pass
        clear(ID,gene)
    return

def main():
    with open('manifest','r') as f:
        files = f.read()
    files = files.split('\n')
    for file in files:
        run(file)
    return

if __name__ == "__main__":
    main() 
