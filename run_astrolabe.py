import os 
import subprocess

BIN = '/home/nat/Documents/tools/astrolabe-0.8.7.0'
ref_bin = '/home/nat/Documents/tools/ref'

def preprocess(bam,ID):
    subprocess.run(['samtools','view','-1','-b','-@','8','-o',f'{ID}.extract.bam',bam,
    '10:96519000-96612771','10:96695000-96752000','22:42522313-42527533'])
    subprocess.run(['samtools','index',f'{ID}.extract.bam'])
    subprocess.run(['gatk','--java-options','-Xmx4g','HaplotypeCaller',
    '-R',f'{ref_bin}/hg19/Homo_sapiens_assembly19.fasta',
    '-I',f'{ID}.extract.bam','-O',f'{ID}.extract.vcf.gz',
    '-L','10:96519000-96612771','-L','10:96695000-96752000',
    '-L','22:42522313-42527533','--verbosity','ERROR'])
    return

def clean(ID):
    os.remove(f'{ID}.extract.bam')
    os.remove(f'{ID}.extract.bam.bai')
    os.remove(f'{ID}.extract.vcf.gz')
    os.remove(f'{ID}.extract.vcf.gz.tbi')
    return

def run(bam):
    ID = os.path.basename(bam)[:-4]
    preprocess(bam,ID)
    print(ID)
    subprocess.run(['bash',f'{BIN}/run-astrolabe.sh', 
    '-inputVCF',f'{ID}.extract.vcf.gz','-skipVcfQC',
    '-inputBam',f'{ID}.extract.bam',
    '-outFile',ID, '-conf', f'{BIN}/astrolabe.ini'])
    clean(ID)
    return

def main():
    with open('manifest','r') as f:
        files = f.read()
    files = files.split('\n')
    for file in files[:-1]:
        run(file)
    return

if __name__ == "__main__":
    main() 