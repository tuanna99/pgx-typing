import os
import subprocess
import glob

roi = '1:47603106-47614526 1:60358979-60392423 7:99354582-99381811\
     7:99425635-99464173 10:94833231-94837641 10:96447881-96612671\
          10:96698414-96749148 10:96796528-96829254 11:14899555-14913751\
               15:51500253-51630795 15:75011882-75017877 19:15988833-16008884\
                    19:41349442-41356352 19:41497203-41524301 19:41594355-41602100\
                         19:41620352-41634281 19:41699114-41713444 22:42522500-42526883'

def extract(file,ID):
    subprocess.run(['samtools','view','-1','-b','-@','8',
    '-o',f'{ID}.extract.bam',file]+roi.split())

    subprocess.run(['samtools','index',f'{ID}.extract.bam'])

    subprocess.run(['bam','bam2FastQ','--in',f'{ID}.extract.bam',
    '--gzip','--firstOut',f'{ID}_extract.1.fq.gz',
    '--secondOut',f'{ID}_extract.2.fq.gz',
    '--unpairedOut',f'{ID}_unpaired.fq.gz'
    ])
    return

def clean(ID):
    for file in glob.glob('*.log') + glob.glob(f'{ID}*'):
        if os.path.isfile(file):
            os.remove(file)
    return

def run(file):
    ID = os.path.basename(file)[:-4]
    extract(file,ID)

    subprocess.run(['hisatgenotype','--base','cyp','--pass-extract',
    '-p','8','--pp','8','--verbose','1','--out-dir',ID,
    '-1',f'{ID}_extract.1.fq.gz','-2',f'{ID}_extract.2.fq.gz'])
        # '-U',f'{ID}_extract.{gene}.unpaired.fq.gz'
    clean(ID)
    return 

def main():
    with open('manifest','r') as f:
        files = f.read()
    files = files.split('\n')
    for file in files[:-1]:
        run(file)
    return

if __name__ == "__main__":
    main()