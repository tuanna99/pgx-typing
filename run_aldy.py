import os 
import subprocess
import glob

def main():
    with open('bin/manifest','r') as f:
        files = f.read()
    files = files.split('\n')
    for file in files[:-1]:
        ID = os.path.basename(file)[:-4]
        subprocess.run(['aldy','genotype','-p','illumina',
        '-o',f'aldy/{ID}',file])
    return

if __name__ == "__main__":
    main()
